﻿using System;
using System.Configuration;

namespace BadBrokerTestTask.Configuration
{
    public class AppConfig
    {
        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString; }
        }

        public static string UrlApi
        {
            get { return ConfigurationManager.AppSettings["URLApi"]; }
        }

        public static string BaseCurrency
        {
            get { return ConfigurationManager.AppSettings["BaseCurrency"]; }
        }

        public static string Symbols
        {
            get { return ConfigurationManager.AppSettings["Symbols"]; }
        }

        public static decimal Fee
        {
            get { return Convert.ToDecimal(ConfigurationManager.AppSettings["Fee"]); }
        }
    }
}
