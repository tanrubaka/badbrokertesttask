﻿using BadBrokerTestTask.Model.DomainObjects;

namespace BadBrokerTestTask.Model.Data
{
    public interface IUnitOfWork
    {
        IRepository<Currency> Currency { get; }
        IRepository<Rate> Rates { get; }
        IRepository<Exchange> Exchanges { get; }
        bool SaveChanges();
        void Discard();
    }
}