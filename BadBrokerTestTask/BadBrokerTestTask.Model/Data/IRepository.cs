﻿using System.Linq;
using BadBrokerTestTask.Model.DomainObjects;

namespace BadBrokerTestTask.Model.Data
{
    public interface IRepository<T>
       where T : IDomainObject, new()
    {
        IQueryable<T> GetAll();
        void Delete(T obj);
        void Add(T obj);
        T Get(int id);
    }
}