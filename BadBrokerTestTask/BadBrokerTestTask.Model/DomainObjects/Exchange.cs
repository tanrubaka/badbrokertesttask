﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace BadBrokerTestTask.Model.DomainObjects
{
    public class Exchange : IDomainObject
    {
        public Exchange()
        {
            Rates = new ObservableCollection<Rate>();
        }

        public int Id { get; set; }
        public DateTime Date { get; set; }

        public int CurrencyId { get; set; }

        [ForeignKey("CurrencyId")]
        public virtual Currency Currency { get; set; }//base currency
        public ObservableCollection<Rate> Rates { get; set; } 
    }
}