﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BadBrokerTestTask.Model.DomainObjects
{
    public class Rate:IDomainObject
    {
        public int Id { get; set; }
        public double Value { get; set; }
        public int CurrencyId { get; set; }
        [ForeignKey("CurrencyId")]
        public virtual Currency Currency { get; set; }
        public int ExchangeId { get; set; }
        [ForeignKey("ExchangeId")]
        public virtual Exchange Exchange { get; set; }
    }
}