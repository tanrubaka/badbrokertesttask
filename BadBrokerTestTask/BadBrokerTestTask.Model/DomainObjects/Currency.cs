﻿using System.Collections.ObjectModel;

namespace BadBrokerTestTask.Model.DomainObjects
{
    public sealed class Currency : IDomainObject
    {
        public Currency()
        {
            Rates = new ObservableCollection<Rate>();
            Exchanges = new ObservableCollection<Exchange>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public ObservableCollection<Rate> Rates { get; set; }
        public ObservableCollection<Exchange> Exchanges { get; set; } 
    }
}