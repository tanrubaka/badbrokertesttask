﻿namespace BadBrokerTestTask.Model.DomainObjects
{
    public interface IDomainObject
    {
        int Id { get; set; } 
    }
}