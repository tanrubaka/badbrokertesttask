﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BadBrokerTestTask.DAL.Models;
using Microsoft.Practices.Unity;

namespace BadBrokerTestTask.Controllers
{
    public class HomeController : Controller
    {
        [Dependency]
        public Logic.IExchange ExchangeLogic { get; set; }

        public ActionResult Index()
        {
            return RedirectToAction("Get");
        }

        public ActionResult Get()
        {
            return View();
        }
        
        public ActionResult Exchange(Models.Form obj)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { errors = ModelState.Values.SelectMany(v => v.Errors.Select(e => e.ErrorMessage)) }, JsonRequestBehavior.AllowGet);
            }

            var model = ExchangeLogic.Get(obj.DateFrom, obj.DateTo);

            var result = ExchangeLogic.Calculate((IList<ExchangeInfo>) model, obj.Money);

            return Json(new {model, result}, JsonRequestBehavior.AllowGet);
           
        }
    }
}
