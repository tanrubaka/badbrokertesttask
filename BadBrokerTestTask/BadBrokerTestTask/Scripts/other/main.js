﻿$(function () {
    $("#DateFrom").datepicker();
    $("#DateTo").datepicker();
});

function Get() {

    var url = '/Home/Exchange';
    $("#Status").text("Loading");
    var $btn = $("#ButtonSubmit").button('loading');

    $.ajax({
        url: url,
        data: $("form").serialize(),
        dataType: 'json',
        type: 'POST',
        success: function (result) {
            if (result.errors) {
                $("#Status").text(result.errors);
                $btn.button('reset');
            }
            else {
                $('#Exchange').html(CreateTable(result.model));
                $('#Result').html(Result(result.result));
                $("#Status").text("Ready");
                $btn.button('reset');
            }
        },
        error: function () {
            $("#Status").text("Error");
            $btn.button('reset');
        }

    });

};

function Result(result) {
    var dateBuy = new Date(parseInt(result.DateBuy.substr(6)));
    var dateSell = new Date(parseInt(result.DateSell.substr(6)));
    var options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
    };
    return dateBuy.toLocaleString("en-US", options) + ' - ' + dateSell.toLocaleString("en-US", options) +
        ' : ' + Round(result.Value) + '(' + result.CurrencyName + ') ';
}

function CreateTable(model) {
    var table = document.createElement("table");
    table.className = "table table-hover table-striped";
    
    
    var header = table.createTHead();
    
    var rowH = header.insertRow(0);

    var cellH = document.createElement('th');
    rowH.appendChild(cellH);
    cellH.innerHTML = "Date";
    
    $.each(model[0].Rates, function (i, rate) {
        var cellValue = document.createElement('th');
        rowH.appendChild(cellValue);
        cellValue.innerHTML = (rate.CurrencyName);
    });
    var tbody = document.createElement('tbody');
    table.appendChild(tbody);
    $.each(model, function (index, element) {
        var row = tbody.insertRow(index );

        var cellDate = row.insertCell(0);

        var date = new Date(parseInt(element.Date.substr(6)));
        var options = {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
        };
        cellDate.innerHTML = date.toLocaleString("en-US", options);
        
        $.each(element.Rates, function (i, rate) {
            var cellValue = row.insertCell(i + 1);
            cellValue.innerHTML = Round(rate.Value);
        });
    });
    return table;
}

function Round(value) {
    return Math.round(value * 100) / 100;
}