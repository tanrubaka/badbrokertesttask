﻿using System.Web;
using System.Web.Mvc;
using BadBrokerTestTask.Filters;

namespace BadBrokerTestTask
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleAllErrorAttribute());
        }
    }
}