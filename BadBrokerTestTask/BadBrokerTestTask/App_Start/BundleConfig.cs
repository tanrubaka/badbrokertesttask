﻿using System.Web.Optimization;

namespace BadBrokerTestTask
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Scripts/css").Include(
                        "~/Content/css/bootstrap.min.css",
                        "~/Content/css/main.css",
                        "~/Content/css/datepicker.css"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                         "~/Scripts/jquery-1.11.3.min.js",
                         "~/Scripts/bootstrap/bootstrap-datepicker.js",
                         "~/Scripts/bootstrap/bootstrap.min.js",
                         "~/Scripts/other/main.js"
                         ));
        }
    }
}