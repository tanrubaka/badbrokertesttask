using System.Web.Mvc;
using BadBrokerTestTask.Controllers;
using Microsoft.Practices.Unity;
using Unity.Mvc4;

namespace BadBrokerTestTask
{
    public static class Bootstrapper
    {
        public static IUnityContainer Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            container.RegisterType<Logic.IExchange, Logic.Exchange>(new HierarchicalLifetimeManager());
            container.RegisterType<DAL.DataContext>(new PerRequestLifetimeManager());
            container.RegisterType<IController, HomeController>("Home");

            Logic.IModule logicModul = new Logic.BlModule(container);
            logicModul.Initialize();

            DAL.IModule dalModul = new DAL.BlModule(container);
            dalModul.Initialize();

            RegisterTypes(container);

            return container;
        }

        public static void RegisterTypes(IUnityContainer container)
        {

        }
    }
}