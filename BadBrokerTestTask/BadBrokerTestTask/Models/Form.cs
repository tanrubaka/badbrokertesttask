﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BadBrokerTestTask.Models
{
    [CustomValidation(typeof(Form), "ValidateDateRange")]
    public class Form
    {
        private const int MaxDaysDistance = 60;

        [Display(Name = "DateFrom", ResourceType = typeof(Resources.Resource))]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}",
                  ApplyFormatInEditMode = true)]
        public DateTime DateFrom { get; set; }

        [Display(Name = "DateTo", ResourceType = typeof(Resources.Resource))]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}",
                  ApplyFormatInEditMode = true)]
        public DateTime DateTo { get; set; }

        [Display(Name = "Money", ResourceType = typeof(Resources.Resource))]
        public decimal Money { get; set; }

        public static ValidationResult ValidateDateRange(Form plot)
        {
            if (plot.DateFrom > plot.DateTo)
            {
                return new ValidationResult(Resources.Resource.ErrorDate);
            }
            if ((plot.DateTo - plot.DateFrom).TotalDays > MaxDaysDistance)
            {
                return new ValidationResult(string.Format(Resources.Resource.ErrorLimitDateRange, MaxDaysDistance));
            }
            if (plot.Money <= 0)
            {
                return new ValidationResult(Resources.Resource.ErrorMoney);
                
            }
            return ValidationResult.Success;
        }
    }
}