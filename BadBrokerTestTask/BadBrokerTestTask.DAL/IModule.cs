﻿namespace BadBrokerTestTask.DAL
{
    public interface IModule
    {
        void Initialize();
    }
}