﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using BadBrokerTestTask.DAL.Exceptions;
using BadBrokerTestTask.DAL.IManagers;
using BadBrokerTestTask.DAL.Models;
using BadBrokerTestTask.Model.Data;
using BadBrokerTestTask.Model.DomainObjects;
using Microsoft.Practices.Unity;

namespace BadBrokerTestTask.DAL.Managers
{
    public class RateManager : IRateManager
    {
        [Dependency]
        public IUnitOfWork Repository { get; set; }

        public RateInfo Get(int id)
        {
            return GetInner(Repository.Rates.Get(id));
        }

        public IList<RateInfo> GetAllByExchange(int id, IList<string> symbols)
        {
            return Repository.Rates.GetAll().Where(item => item.ExchangeId == id && symbols.Contains(item.Currency.Name))
                .Select(item => new RateInfo
                {
                    Id = item.Id,
                    CurrencyId = item.CurrencyId,
                    CurrencyName = item.Currency.Name,
                    ExchangeId = item.ExchangeId,
                    Value = item.Value
                }).ToList();
        }

        public IList<RateInfo> GetAll()
        {
            return Repository.Rates.GetAll().Select(GetInner).ToList();
        }

        public int Add(RateInfo obj)
        {
            var newObject = new Rate
            {
                ExchangeId = obj.ExchangeId,
                Value = obj.Value,
                CurrencyId = obj.CurrencyId
            };
            Repository.Rates.Add(newObject);
            Repository.SaveChanges();
            return newObject.Id;
        }

        public void Edit(RateInfo obj)
        {
            var editObject = Repository.Rates.Get(obj.Id);
            if (editObject != null)
            {
                editObject.ExchangeId = obj.ExchangeId;
                editObject.Value = obj.Value;
                editObject.CurrencyId = obj.CurrencyId;
                Repository.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            var deletedObject = Repository.Rates.Get(id);
            if (deletedObject == null)
            {
                return;
            }
            Repository.Rates.Delete(deletedObject);
            try
            {
                Repository.SaveChanges();
            }
            catch (SqlException ex)
            {
                ExceptionHelper.RaiseIfForeignKeyViolation(ex, Resources.Resource.ErrorDelete,
                    string.Format("ID = {0}.", id));

                throw;
            }
        }

        private RateInfo GetInner(Rate obj)
        {
            return new RateInfo
            {
                Id = obj.Id,
                CurrencyId = obj.CurrencyId,
                ExchangeId = obj.ExchangeId,
                Value = obj.Value
            };
        } 
    }
}