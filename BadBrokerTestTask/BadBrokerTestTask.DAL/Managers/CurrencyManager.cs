﻿using System.Collections.Generic;
using System.Data.SqlClient;
using BadBrokerTestTask.DAL.Exceptions;
using BadBrokerTestTask.DAL.IManagers;
using BadBrokerTestTask.DAL.Models;
using BadBrokerTestTask.Model.Data;
using BadBrokerTestTask.Model.DomainObjects;
using System.Linq;
using Microsoft.Practices.Unity;

namespace BadBrokerTestTask.DAL.Managers
{
    public class CurrencyManager : ICurrencyManager
    {
        [Dependency]
        public IUnitOfWork Repository { get; set; }

        public CurrencyInfo Load(string name)
        {
            var currency = Get(name);
            if (currency == null)
            {
                currency = new CurrencyInfo
                {
                    Name = name
                };
                currency.Id = Add(currency);
            }
            return currency;
        }

        private CurrencyInfo Get(string name)
        {
            return Repository.Currency.GetAll()
                .Where(item => item.Name == name)
                .Select(GetInner)
                .FirstOrDefault();
        }

        public CurrencyInfo Get(int id)
        {
            return GetInner(Repository.Currency.Get(id));
        }

        public IList<CurrencyInfo> GetAll()
        {
            return Repository.Currency.GetAll().Select(GetInner).ToList();
        }

        public int Add(CurrencyInfo obj)
        {
            var newObject = new Currency
                {
                    Name = obj.Name
                };
            Repository.Currency.Add(newObject);
            Repository.SaveChanges();
            return newObject.Id;
        }

        public void Edit(CurrencyInfo obj)
        {
            var editObject = Repository.Currency.Get(obj.Id);
            if (editObject!=null)
            {
                editObject.Name = obj.Name;
                Repository.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            var deletedObject = Repository.Currency.Get(id);
            if (deletedObject == null)
            {
                return;
            }
            Repository.Currency.Delete(deletedObject);

            try
            {
                Repository.SaveChanges();
            }
            catch (SqlException ex)
            {
                ExceptionHelper.RaiseIfForeignKeyViolation(ex, Resources.Resource.ErrorDelete,
                    string.Format("ID = {0}.", id));

                throw;
            }
        }

        private CurrencyInfo GetInner(Currency obj)
        {
            return new CurrencyInfo
                {
                    Id = obj.Id,
                    Name = obj.Name
                };
        }
    }
}