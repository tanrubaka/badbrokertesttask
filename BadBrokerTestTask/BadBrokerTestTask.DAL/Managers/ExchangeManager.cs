﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using BadBrokerTestTask.DAL.Exceptions;
using BadBrokerTestTask.DAL.IManagers;
using BadBrokerTestTask.DAL.Models;
using BadBrokerTestTask.Model.Data;
using BadBrokerTestTask.Model.DomainObjects;
using System.Linq;
using Microsoft.Practices.Unity;

namespace BadBrokerTestTask.DAL.Managers
{
    public class ExchangeManager : IExchangeManager
    {
        [Dependency]
        public IUnitOfWork Repository { get; set; }

        public ExchangeInfo Get(int id)
        {
            return GetInner(Repository.Exchanges.Get(id));
        }

        public ExchangeInfo Get(DateTime date, int curencyId)
        {
            return Repository.Exchanges.GetAll().Where(item => item.Date == date && item.CurrencyId == curencyId)
                .Select(item=> new ExchangeInfo
                    {
                        Id = item.Id,
                        Date = item.Date,
                        CurrencyId = item.CurrencyId,
                        CurrencyName = item.Currency.Name
                    }).FirstOrDefault();
        }
        
        public int Add(ExchangeInfo obj)
        {
            var newObject = new Exchange
                {
                    Date = obj.Date,
                    CurrencyId = obj.CurrencyId
                };
            Repository.Exchanges.Add(newObject);
            Repository.SaveChanges();
            return newObject.Id;
        }

        public void Edit(ExchangeInfo obj)
        {
            var editObject = Repository.Exchanges.Get(obj.Id);
            if (editObject!=null)
            {
                editObject.Date = obj.Date;
                editObject.CurrencyId = obj.CurrencyId;
                Repository.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            var deletedObject = Repository.Exchanges.Get(id);
            if (deletedObject == null)
            {
                return;
            }
            Repository.Exchanges.Delete(deletedObject); 
            try
            {
                Repository.SaveChanges();
            }
            catch (SqlException ex)
            {
                ExceptionHelper.RaiseIfForeignKeyViolation(ex, Resources.Resource.ErrorDelete,
                    string.Format("ID = {0}.", id));

                throw;
            }
        }

        private ExchangeInfo GetInner(Exchange obj)
        {
            return new ExchangeInfo
                {
                    Id = obj.Id,
                    Date = obj.Date,
                    CurrencyId = obj.CurrencyId
                };
        }
    }
}