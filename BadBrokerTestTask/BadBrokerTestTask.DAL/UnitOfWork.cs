﻿using System;
using BadBrokerTestTask.Model.Data;
using BadBrokerTestTask.Model.DomainObjects;
using Microsoft.Practices.Unity;

namespace BadBrokerTestTask.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        [Dependency]
        public DataContext Context { get; set; }

        [Dependency]
        public IRepository<Currency> Currency { get; set; }

        [Dependency]
        public IRepository<Rate> Rates { get; set; }

        [Dependency]
        public IRepository<Exchange> Exchanges { get; set; }

        public bool SaveChanges()
        {
            try
            {
                Context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Discard()
        {
            Context.Dispose();
            Context = new DataContext();
        }
    }
}