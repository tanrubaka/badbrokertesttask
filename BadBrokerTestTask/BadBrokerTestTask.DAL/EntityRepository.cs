﻿using System.Linq;
using BadBrokerTestTask.Model.Data;
using BadBrokerTestTask.Model.DomainObjects;
using Microsoft.Practices.Unity;

namespace BadBrokerTestTask.DAL
{
    internal class EntityRepository<T> : IRepository<T> where T : class, IDomainObject, new()
    {
        [Dependency]
        public DataContext Context { get; set; }

        public IQueryable<T> GetAll()
        {
            return Context.Set<T>();
        }

        public void Delete(T obj)
        {
            Context.Set<T>().Remove(obj);
        }

        public void Add(T obj)
        {
            Context.Set<T>().Add(obj);
        }

        public T Get(int id)
        {
            return Context.Set<T>().SingleOrDefault(o => o.Id == id);
        }
    }
}