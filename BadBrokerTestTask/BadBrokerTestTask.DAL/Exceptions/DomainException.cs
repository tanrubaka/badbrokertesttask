﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BadBrokerTestTask.DAL.Exceptions
{
    [Serializable]
    public class DomainException : Exception
    {
        public DomainException(string message) : base(message) { }

        public DomainException(string message, Exception inner) : base(message, inner) { }

        public DomainException(string message, string details)
            : this(message, details, null)
        {
        }

        public DomainException(string message, string details, Exception inner)
            : base(message, inner)
        {
            Details = details;
        }

        protected DomainException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }

        public string Details { get; private set; }
    }
}
