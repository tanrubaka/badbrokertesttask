﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BadBrokerTestTask.DAL.Exceptions
{
    [Serializable]
    public class ValidationException : Exception
    {
        private IList<string> _errors;

        public ValidationException(string error)
        {
            _errors = new List<string> { error };
        }

        public ValidationException(IEnumerable<string> errors)
        {
            _errors = new List<string>(errors);
        }

        protected ValidationException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }

        public override string Message
        {
            get { return string.Join(";", _errors); }
        }

        public IList<string> Errors
        {
            get { return _errors; }
        }
    }
}
