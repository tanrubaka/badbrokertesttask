﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace BadBrokerTestTask.DAL.Exceptions
{
    internal class ExceptionHelper
    {
        public static void RaiseIfForeignKeyViolation(SqlException ex, string message, string details)
        {
            if (ex.Number == SqlExceptionCodes.ForeignKeyViolation)
            {
                throw new DomainException(message, details, ex);
            }
        }
    }
}
