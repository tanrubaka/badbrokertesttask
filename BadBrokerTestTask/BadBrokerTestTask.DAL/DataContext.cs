﻿using System.Data.Entity;
using BadBrokerTestTask.Configuration;
using BadBrokerTestTask.Model.DomainObjects;

namespace BadBrokerTestTask.DAL
{
    public class DataContext : DbContext
    {
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Rate> Rates { get; set; }
        public DbSet<Exchange> Exchanges { get; set; }

        public DataContext()
            : base(AppConfig.ConnectionString)
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Rate>().HasRequired(item => item.Currency)
                        .WithMany(item => item.Rates)
                        .HasForeignKey(item => item.CurrencyId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rate>().HasRequired(item => item.Exchange)
                        .WithMany(item => item.Rates)
                        .HasForeignKey(item => item.ExchangeId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Exchange>().HasRequired(item => item.Currency)
                        .WithMany(item => item.Exchanges)
                        .HasForeignKey(item => item.CurrencyId)
                        .WillCascadeOnDelete(false);
        }
    }
}