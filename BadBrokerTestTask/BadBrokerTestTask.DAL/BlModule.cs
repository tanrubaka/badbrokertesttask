﻿using BadBrokerTestTask.Model.Data;
using BadBrokerTestTask.Model.DomainObjects;
using Microsoft.Practices.Unity;

namespace BadBrokerTestTask.DAL
{
    public class BlModule : IModule
    {
        private readonly IUnityContainer _container;

        public BlModule(IUnityContainer container)
        {
            _container = container;
        }

        public void Initialize()
        {
            _container
                .RegisterType<IRepository<Currency>, EntityRepository<Currency>>(new HierarchicalLifetimeManager())
                .RegisterType<IRepository<Rate>, EntityRepository<Rate>>(new HierarchicalLifetimeManager())
                .RegisterType<IRepository<Exchange>, EntityRepository<Exchange>>(new HierarchicalLifetimeManager())
                .RegisterType<IUnitOfWork, UnitOfWork>(new HierarchicalLifetimeManager());
        }
    }
}