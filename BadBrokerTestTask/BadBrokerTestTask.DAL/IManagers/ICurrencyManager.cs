﻿using System.Collections.Generic;
using BadBrokerTestTask.DAL.Models;
using BadBrokerTestTask.Model.DomainObjects;

namespace BadBrokerTestTask.DAL.IManagers
{
    public interface ICurrencyManager
    {
        CurrencyInfo Load(string name);
        CurrencyInfo Get(int id);
        IList<CurrencyInfo> GetAll();
        int Add(CurrencyInfo obj);
        void Edit(CurrencyInfo obj);
        void Delete(int id);
    }
}