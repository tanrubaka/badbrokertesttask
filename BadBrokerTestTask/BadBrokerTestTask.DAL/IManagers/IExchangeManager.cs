﻿using System;
using BadBrokerTestTask.DAL.Models;

namespace BadBrokerTestTask.DAL.IManagers
{
    public interface IExchangeManager
    {
        ExchangeInfo Get(int id);
        ExchangeInfo Get(DateTime date, int curencyId);
        int Add(ExchangeInfo obj);
        void Edit(ExchangeInfo obj);
        void Delete(int id);
    }
}