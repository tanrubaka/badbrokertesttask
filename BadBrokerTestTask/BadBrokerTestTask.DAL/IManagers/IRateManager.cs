﻿using System.Collections.Generic;
using BadBrokerTestTask.DAL.Models;

namespace BadBrokerTestTask.DAL.IManagers
{
    public interface IRateManager
    {
        RateInfo Get(int id);
        IList<RateInfo> GetAllByExchange(int id, IList<string> symbols);
        IList<RateInfo> GetAll();
        int Add(RateInfo obj);
        void Edit(RateInfo obj);
        void Delete(int id);
    }
}