﻿namespace BadBrokerTestTask.DAL.Models
{
    public class CurrencyInfo
    {
        public int Id { get; set; }
        public string Name { get; set; } 
    }
}