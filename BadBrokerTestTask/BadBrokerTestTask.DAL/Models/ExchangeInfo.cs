﻿using System;
using System.Collections.Generic;

namespace BadBrokerTestTask.DAL.Models
{
    public class ExchangeInfo
    {
        public ExchangeInfo()
        {
            Rates = new List<RateInfo>();
        }

        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int CurrencyId { get; set; } //base currency
        public string CurrencyName { get; set; }
        public List<RateInfo> Rates { get; set; }
    }
}