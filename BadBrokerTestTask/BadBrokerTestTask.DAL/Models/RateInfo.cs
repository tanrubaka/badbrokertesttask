﻿using System;

namespace BadBrokerTestTask.DAL.Models
{
    public class RateInfo
    {
        public int Id { get; set; }
        public int ExchangeId { get; set; }
        public double Value { get; set; }
        public int CurrencyId { get; set; }
        public string CurrencyName { get; set; }
    }
}