﻿using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

namespace BadBrokerTestTask.Utils
{
    public class Fixer : IFixer
    {
        public ExchangeRates Get(string baseName,DateTime date, IList<string> symbols)
        {
            var webClient = new WebClient();
            var request = WebRequest.Create(Configuration.AppConfig.UrlApi);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Proxy.Credentials = CredentialCache.DefaultCredentials;

            var response = webClient.DownloadString(
                string.Format("{0}{1}?base={2}&symbols={3}", 
                    Configuration.AppConfig.UrlApi,
                    date.ToString("yyyy-MM-dd"), 
                    baseName, 
                    string.Join(",", symbols)));

            var exchangeRates = JsonConvert.DeserializeObject<ExchangeRates>(response);

            return exchangeRates;
        }

        public ExchangeRates Get(string baseName, DateTime date)
        {
            var webClient = new WebClient();
            var request = WebRequest.Create(Configuration.AppConfig.UrlApi);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.Proxy.Credentials = CredentialCache.DefaultCredentials;

            var response = webClient.DownloadString(
                string.Format("{0}{1}?base={2}", 
                    Configuration.AppConfig.UrlApi,
                    date.ToString("yyyy-MM-dd"), 
                    baseName));

            var exchangeRates = JsonConvert.DeserializeObject<ExchangeRates>(response);

            return exchangeRates;
        }
    }
}