﻿using System;
using System.Collections.Generic;

namespace BadBrokerTestTask.Utils
{
    public interface IFixer
    {
        ExchangeRates Get(string baseName, DateTime date, IList<string> symbols);
        ExchangeRates Get(string baseName, DateTime date);
    }
}