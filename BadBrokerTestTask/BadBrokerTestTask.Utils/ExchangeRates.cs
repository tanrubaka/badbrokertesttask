﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BadBrokerTestTask.Utils
{
    public class ExchangeRates
    {
        [JsonProperty("date")]
        public string Date { get; set; }
        [JsonProperty("base")]
        public string Base { get; set; }
        [JsonProperty("rates")]
        public Dictionary<string, float> Rates { get; set; }
    }
}