﻿using System;
using System.Collections.Generic;
using BadBrokerTestTask.DAL.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BadBrokerTestTask.Tests
{
    [TestClass]
    public class UnitTestLogic
    {
        [TestMethod]
        public void TestCalculate()
        {
            var container = new UnityContainer();

            container.RegisterType<Logic.IExchange, Logic.Exchange>(new HierarchicalLifetimeManager());
            container.RegisterType<DAL.DataContext>(new ContainerControlledLifetimeManager());
            Logic.IModule logicModul = new Logic.BlModule(container);
            logicModul.Initialize();

            DAL.IModule dalModul = new DAL.BlModule(container);
            dalModul.Initialize();

            var exchange = container.Resolve<Logic.IExchange>();
            var res = exchange.Calculate(SetValues(), 50);
            
        }

        private static IList<DAL.Models.ExchangeInfo> SetValues()
        {
            var res = new List<DAL.Models.ExchangeInfo>
                {
                    new ExchangeInfo
                        {
                            CurrencyName = "USA",
                            Date = new DateTime(2012, 1, 5),
                            Rates = new List<RateInfo>()
                                {
                                    new RateInfo {CurrencyName = "RUB", Value = 40}
                                }
                        },
                    new ExchangeInfo
                        {
                            CurrencyName = "USA",
                            Date = new DateTime(2012, 1, 7),
                            Rates = new List<RateInfo>()
                                {
                                    new RateInfo {CurrencyName = "RUB", Value = 35}
                                }
                        },
                    new ExchangeInfo
                        {
                            CurrencyName = "USA",
                            Date = new DateTime(2012, 1, 19),
                            Rates = new List<RateInfo>()
                                {
                                    new RateInfo {CurrencyName = "RUB", Value = 30}
                                }
                        }
                };
            return res;
        }
    }
}
