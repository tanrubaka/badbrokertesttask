﻿using BadBrokerTestTask.DAL;
using BadBrokerTestTask.DAL.IManagers;
using BadBrokerTestTask.DAL.Managers;
using BadBrokerTestTask.Utils;
using Microsoft.Practices.Unity;

namespace BadBrokerTestTask.Logic
{
    public class BlModule : IModule
    {
        private readonly IUnityContainer _container;

        public BlModule(IUnityContainer container)
        {
            _container = container;
        }

        public void Initialize()
        {
            _container
                .RegisterType<IRateManager, RateManager>(new HierarchicalLifetimeManager())
                .RegisterType<ICurrencyManager, CurrencyManager>(new HierarchicalLifetimeManager())
                .RegisterType<IExchangeManager, ExchangeManager>(new HierarchicalLifetimeManager())
                .RegisterType<IFixer, Fixer>(new HierarchicalLifetimeManager());
        }
    }
}