﻿using System;

namespace BadBrokerTestTask.Logic
{
    public class Result
    {
        public double Value { get; set; }
        public string CurrencyName { get; set; }
        public DateTime DateBuy { get; set; }
        public DateTime DateSell { get; set; }
    }
}