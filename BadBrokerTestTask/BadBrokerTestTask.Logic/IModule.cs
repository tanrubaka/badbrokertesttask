﻿namespace BadBrokerTestTask.Logic
{
    public interface IModule
    {
        void Initialize();
    }
}