﻿using System;
using System.Collections.Generic;
using BadBrokerTestTask.DAL.Models;

namespace BadBrokerTestTask.Logic
{
    public interface IExchange
    {
        IEnumerable<ExchangeInfo> Get(DateTime dateFrom, DateTime dateTo);
        Result Calculate(IList<ExchangeInfo> exchanges, decimal money);
    }
}