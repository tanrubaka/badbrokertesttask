﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BadBrokerTestTask.DAL.IManagers;
using BadBrokerTestTask.DAL.Managers;
using BadBrokerTestTask.DAL.Models;
using BadBrokerTestTask.Utils;
using Microsoft.Practices.Unity;

namespace BadBrokerTestTask.Logic
{
    public class Exchange : IExchange
    {
        [Dependency]
        public ICurrencyManager CurrencyManager { get; set; }
        [Dependency]
        public IExchangeManager ExchangeManager { get; set; }
        [Dependency]
        public IRateManager RateManager { get; set; }
        [Dependency]
        public IFixer Fixer { get; set; }

        private ExchangeInfo GetFromFixer(CurrencyInfo currency, DateTime date, IList<string> symbols)
        {
            //to receive from the service http://fixer.io/
            var rate = Fixer.Get(currency.Name, date, symbols);
            if (rate == null)
            {
                return null;
            }

            var exchange = new ExchangeInfo
                {
                    Date = date,
                    CurrencyId = currency.Id
                };

            exchange.Id = ExchangeManager.Add(exchange);
            foreach (var f in rate.Rates)
            {
                var c = CurrencyManager.Load(f.Key);
                var r = new RateInfo
                {
                    ExchangeId = exchange.Id,
                    Value = f.Value,
                    CurrencyId = c.Id,
                    CurrencyName = f.Key
                };
                r.Id = RateManager.Add(r);
                exchange.Rates.Add(r);
            }
            return exchange;
        }

        public IEnumerable<ExchangeInfo> Get(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                //get currency
                var currency = CurrencyManager.Load(Configuration.AppConfig.BaseCurrency);

                var exchanges = new List<ExchangeInfo>();
                var date = dateFrom;
                var symbols = Configuration.AppConfig.Symbols.Split(',').ToList();
                while (date <= dateTo)
                {
                    //get exchange
                    var exchange = ExchangeManager.Get(date, currency.Id);
                    if (exchange == null)
                    {
                        exchange = GetFromFixer(currency, date, symbols);
                    }
                    else
                    {
                        exchange.Rates = RateManager.GetAllByExchange(exchange.Id, symbols).ToList();
                    }
                    if (exchange != null && exchanges.All(item => item.Date != exchange.Date))
                    {
                        exchanges.Add(exchange);
                    }
                    date = date.AddDays(1);
                }
                return exchanges;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Result Calculate(IList<ExchangeInfo> exchanges, decimal money)
        {
            try
            {
                var resList = new List<Result>();

                foreach (var exchangeBuy in exchanges.OrderBy(item=> item.Date))
                {
                    foreach (var exchangeSell in exchanges.Where(item => item.Date > exchangeBuy.Date))
                    {
                        foreach (var valueBuy in exchangeBuy.Rates)
                        {
                            var valueSell = exchangeSell.Rates.FirstOrDefault(o => o.CurrencyId == valueBuy.CurrencyId);
                            if (valueBuy != null && valueSell != null)
                            {
                                var res = (valueBuy.Value*((double) money)/valueSell.Value) -
                                          (exchangeSell.Date - exchangeBuy.Date).TotalDays * (double) Configuration.AppConfig.Fee;
                                resList.Add(new Result
                                    {
                                        Value = res,
                                        CurrencyName = valueBuy.CurrencyName,
                                        DateBuy = exchangeBuy.Date,
                                        DateSell = exchangeSell.Date
                                    });
                            }
                        }
                    }
                }
                return resList.OrderByDescending(item => item.Value).FirstOrDefault();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
